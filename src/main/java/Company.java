import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Company {
    // Create Warrenty Book
    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<WarrentBookEntry> warrentybook = new ArrayList<WarrentBookEntry>();
        warrentybook.add(new WarrentBookEntry("besta","2"));

        int wybor = 0;
        while (true) {
            System.out.println("----------------------------");
            System.out.println("Menu: ");
            System.out.println("1. Wyswietl calosc ");
            System.out.println("2. Dodaj rekord ");
            System.out.println("3. Usun rekord");
            System.out.println("4. Znajdz rekord");
            System.out.println("5. Zmien rekord");
            System.out.println("6. Wylacz program");
            System.out.println("----------------------------");

            try {
                @SuppressWarnings("resouce")
                Scanner odczyt = new Scanner(System.in);
                System.out.println("Wybor: ");
                wybor = odczyt.nextInt();

                switch (wybor) {
                    case 1: {
                        displayEntries(warrentybook);
                        fileSave(warrentybook);
                        break;
                    }
                    case 2: {
                        add(warrentybook);
                        break;
                    }
                    case 3: {
                        delete(warrentybook);
                        break;
                    }
                    case 4: {
                        find(warrentybook);
                        break;
                    }
                    case 5: {
                        update(warrentybook);
                        break;
                    }
                    case 6: {
                        System.exit(0);
                        break;
                    }
                    default: {
                        System.out.println("Zla opcja, wybierz inna : ");
                        //odczyt.close();
                        break;
                    }
                }
            } catch (NoSuchElementException e) {
                System.out.println("\n#ERROR# [EXCEPTION] No Such Element Exception\n");
            }
        }
    }

    private static void deleteEntry(int i, ArrayList<WarrentBookEntry> warrentBookEntries) throws FileNotFoundException {
        try {
            WarrentBookEntry entry = warrentBookEntries.get(i);
            System.out.println("Element do usuniecia to : " + entry.getNameCompany() + "\t" + "\t" + entry.getDateINPUT());
            warrentBookEntries.remove(i);
            System.out.println("Usuniete element. ( o indeksie : " + i + " )");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("\n #ERROR# [EXCEPTION] Array index out of bounds\n");
        }
        fileSave(warrentBookEntries);
    }

    private static void updateEntry(int i, ArrayList<WarrentBookEntry> warrentBookEntries) throws FileNotFoundException {
        String nameCompany, dateINPUT;
        @SuppressWarnings("resouce")
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj nazwe firmy: ");
        nameCompany = odczyt.nextLine();
        System.out.println("Podaj date");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("DD-MM-YYYY");
        dateINPUT = odczyt.nextLine(); //date input
        LocalDate localDate = LocalDate.parse(dateINPUT, dateTimeFormatter); //sout Local date

        try {
            warrentBookEntries.set(i, new WarrentBookEntry(nameCompany, dateINPUT));
        } catch (IndexOutOfBoundsException e) {
            System.out.println("\n #ERROR# [EXCEPTION] Array index out of bounds\n");
        }
        //odczyt.close();
        fileSave(warrentBookEntries);
    }

    private static void findElement(String wzorzec, ArrayList<WarrentBookEntry> warrentBookEntries) {
        int znaleziono = 0;
        for (int i = 0; i < warrentBookEntries.size(); i++) {
            WarrentBookEntry entry = warrentBookEntries.get(i);
            if (entry.getNameCompany().equalsIgnoreCase(wzorzec) || entry.getDateINPUT().equalsIgnoreCase(wzorzec)) {
                System.out.println("Szukany element znajduje sie na liscie pod indeksem " + i);
                znaleziono = 1;
                if (znaleziono == 1) {
                    break;
                }
            }
        }
        if (znaleziono != 1) {
            System.out.println("Szukanego elementu nie ma w bazie!");
        }
        //wywołanie
        //findElement("abc",warrentBookEntries);
    }

    @SuppressWarnings("resouce")
    private static void select(int i, ArrayList<WarrentBookEntry> warrentBookEntries) {
        WarrentBookEntry entry = warrentBookEntries.get(i);
        System.out.println("Szukany element to : " + entry.getNameCompany() + "\t" + entry.getDateINPUT());
        //wywolanie
        //select(3,warrnetBookEntries);
    }

    private static void displayEntries(ArrayList<WarrentBookEntry> warrentBookEntries) {
        System.out.println("----------------------------");
        System.out.println("Name Comapny Date warrenty");
        System.out.println("----------------------------");
        for (int i = 0; i < warrentBookEntries.size(); i++) {
            WarrentBookEntry entry = warrentBookEntries.get(i);
            System.out.println(entry.getNameCompany() + "\t\t" + entry.getDateINPUT());
        }
        //wywolanie
        //displayEntries(warrentBookEntries);
    }

    private static void addUserEntry(ArrayList<WarrentBookEntry> warrentBookEntries) throws FileNotFoundException {
        String nameCompany, dateINPUT;
        @SuppressWarnings("resouce")
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj nazwe firmy: ");
        nameCompany = odczyt.nextLine();
        System.out.println("Podaj date");

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("DD-MM-YYYY");
        dateINPUT = odczyt.nextLine(); //date input
        System.out.println(dateINPUT);
        LocalDate localDate = LocalDate.parse(dateINPUT, dateTimeFormatter); //sout Local date
        warrentBookEntries.add(new WarrentBookEntry(nameCompany, dateINPUT));

        //odczyt.close();

        System.out.println("Dodano wpis.\n" + " Nazwa firmy " + nameCompany + "Data" + dateINPUT);
        //wywolanie (add z danymi od uzytkownika)
        //addUserEntry(warrentBookEntries);
        fileSave(warrentBookEntries);
    }

    //Funckje do swith case w mainie

    private static void update(ArrayList<WarrentBookEntry> warrentBookEntries) throws FileNotFoundException {
        //update do swith case
        int i = 0;
        @SuppressWarnings("resouce")
        Scanner czytaj = new Scanner(System.in);
        System.out.println("Podaj indeks elementu ktory chesz zmienic: ");
        i = czytaj.nextInt();
        updateEntry(i, warrentBookEntries);
        fileSave(warrentBookEntries);
        //czytaj.close();
    }

    private static void find(ArrayList<WarrentBookEntry> warrentBookEntries) {
        //podawanie wzorca ( findElement ) do swith case
        @SuppressWarnings("resouce")
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj wzorzec ( nazwe firmy lub date : ");
        String wzorzec = odczyt.nextLine();
        findElement(wzorzec, warrentBookEntries);
        //odczyt.close();
    }

    private static void add(ArrayList<WarrentBookEntry> warrentBookEntries) throws FileNotFoundException {
        addUserEntry(warrentBookEntries);
    }

    private static void delete(ArrayList<WarrentBookEntry> warrentBookEntries) throws FileNotFoundException {
        @SuppressWarnings("resouce")
        Scanner czytaj = new Scanner(System.in);
        System.out.println("Podaj element do usuniecia : ");
        int ktory = czytaj.nextInt();
        deleteEntry(ktory, warrentBookEntries);
        //czytaj.close();
    }

    private static void fileSave(ArrayList<WarrentBookEntry> warrentBookEntries) throws FileNotFoundException {
        String tempFile = "Baza.txt";
        File fileTemp = new File(tempFile);
        if (fileTemp.exists()) {
            fileTemp.delete();
        }
        PrintWriter zapis = new PrintWriter("Baza.txt");
        zapis.println("----------------------------");
        zapis.println("NAME COMPANY\t\t Date");
        zapis.println("----------------------------");
        for (int i = 0; i < warrentBookEntries.size(); i++) {
            WarrentBookEntry entry = warrentBookEntries.get(i);
            zapis.println(entry.getNameCompany() + "\t\t" + entry.getDateINPUT());
        }
        zapis.close();
    }

}

class WarrentBookEntry {
    private String nameCompany;
    // private int nipCompany;
    private String dateINPUT;

    public WarrentBookEntry(String nameCompany, String dateINPUT) {
        this.nameCompany = nameCompany;
        this.dateINPUT = dateINPUT;
    }
//    public WarrentBookEntry(String a, String b) {
//        nameCompany = a;
//        dateINPUT = b;
//    }

    public String getNameCompany() {
        return nameCompany;
    }

    public String getDateINPUT() {
        return dateINPUT;
    }

//   d
    // public int getNipCompany() {
//        return nipCompany;
//    }
}